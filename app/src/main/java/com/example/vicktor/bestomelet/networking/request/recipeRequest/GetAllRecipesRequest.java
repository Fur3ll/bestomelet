package com.example.vicktor.bestomelet.networking.request.recipeRequest;

import com.example.vicktor.bestomelet.model.RecipeModel;
import com.example.vicktor.bestomelet.model.RecipeResponse;
import com.example.vicktor.bestomelet.networking.NetworkHelper;
import com.example.vicktor.bestomelet.networking.request.BaseRequest;

import retrofit2.Call;

public class GetAllRecipesRequest extends BaseRequest<RecipeResponse> {

    @Override
    public Call<RecipeResponse> getCall() {
        return NetworkHelper.getInstance().getRecipeService().getRecipes(null,null,null);
    }
}
