package com.example.vicktor.bestomelet.networking.listener;



public interface RequestListener<T> {

    void onSuccess(T result);

    void onError(int code, String message);

}
