package com.example.vicktor.bestomelet.networking;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkHelper {

    private static final NetworkHelper INSTANCE = new NetworkHelper();

    private final static String BASE_URL = "http://www.recipepuppy.com";
    private static final int TIMEOUT = 30;
    private Retrofit retrofit = null;
    private OkHttpClient okHttpClient;
    private RecipeService recipeService;

    public static NetworkHelper getInstance() {
        return INSTANCE;
    }

    private NetworkHelper() {
        Gson gson = new GsonBuilder().setLenient().create();
        GsonConverterFactory factory = GsonConverterFactory.create(gson);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(TIMEOUT, TimeUnit.SECONDS);
        builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                final Request.Builder request = original.newBuilder();
                return chain.proceed(request.build());
            }
        });

        okHttpClient = builder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(factory)
                .build();
        recipeService = retrofit.create(RecipeService.class);
    }

    public RecipeService getRecipeService() {
        return recipeService;
    }
}
