package com.example.vicktor.bestomelet.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.vicktor.bestomelet.R;
import com.example.vicktor.bestomelet.database.DatabaseManager;
import com.example.vicktor.bestomelet.model.RecipeModel;
import com.example.vicktor.bestomelet.model.RecipeResponse;
import com.example.vicktor.bestomelet.networking.listener.RequestListener;
import com.example.vicktor.bestomelet.networking.request.recipeRequest.GetAllRecipesRequest;
import com.example.vicktor.bestomelet.networking.request.recipeRequest.SearchRecipeRequest;
import com.example.vicktor.bestomelet.ui.adapter.RecipeAdapter;
import com.example.vicktor.bestomelet.utils.ConnectionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity implements RecipeAdapter.OnRecipeItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.empty_tv)
    TextView textView;


    private RecyclerView itemRv;
    private RecipeAdapter recipeAdapter;
    private List<RecipeModel> recipeModels = new ArrayList<>();
    private ConnectionManager connectionManager;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectionManager = new ConnectionManager(this);
        setToolbarTitle();
        setupAdapterAndRecycler();
        setupSearchView();
        requestForRecipe();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void requestForRecipe() {
        progressBar.setVisibility(View.VISIBLE);
        if (connectionManager.isNetworkConnected()) {
            GetAllRecipesRequest request = new GetAllRecipesRequest();
            request.setListener(new RequestListener<RecipeResponse>() {
                @Override
                public void onSuccess(RecipeResponse result) {
                    if (result != null && result.getResults() != null) {
                        recipeModels.clear();
                        recipeModels.addAll(result.getResults());
                        recipeAdapter.notifyDataSetChanged();
                        saveResultToDB(recipeModels);
                        showEmptyMessage(recipeModels);
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    progressBar.setVisibility(View.GONE);
                    showToast(message);
                }
            });
            request.execute();
        } else {
            progressBar.setVisibility(View.GONE);
            recipeModels.clear();
            if (DatabaseManager.getAllRecipes(this) != null) {
                recipeModels.addAll(DatabaseManager.getAllRecipes(this));
                recipeAdapter.notifyDataSetChanged();
                showEmptyMessage(recipeModels);
            }
        }
    }

    private void showEmptyMessage(@NonNull List<RecipeModel> list) {
        if (list.isEmpty()) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    private void saveResultToDB(@NonNull List<RecipeModel> list) {
        for (RecipeModel model : list) {
            DatabaseManager.saveRecipes(this, model);
        }
    }

    private void searchRecipeRequest(@NonNull String search) {
        progressBar.setVisibility(View.VISIBLE);
        if (connectionManager.isNetworkConnected()) {
            SearchRecipeRequest request = new SearchRecipeRequest(search);
            request.setListener(new RequestListener<RecipeResponse>() {
                @Override
                public void onSuccess(RecipeResponse result) {
                    if (result != null && result.getResults() != null) {
                        recipeModels.clear();
                        recipeModels.addAll(result.getResults());
                        recipeAdapter.notifyDataSetChanged();
                        saveResultToDB(recipeModels);
                        progressBar.setVisibility(View.GONE);
                        showEmptyMessage(recipeModels);
                    }
                }

                @Override
                public void onError(int code, String message) {
                    progressBar.setVisibility(View.GONE);
                    showToast(message);
                }
            });
            request.execute();
        } else {
            progressBar.setVisibility(View.GONE);

            List<RecipeModel> recipes = DatabaseManager.getSearchResult(this, search);
            if (recipes != null) {
                recipeModels.clear();
                recipeModels.addAll(recipes);
                recipeAdapter.notifyDataSetChanged();
            } else {
                showToast("No results");
            }
        }
    }

    private void setupSearchView() {
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.findViewById(android.support.v7.appcompat.R.id.search_plate)
                .setBackgroundColor(getResources().getColor(android.R.color.transparent));


        searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery("", true);
                searchView.clearFocus();
                requestForRecipe();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchRecipeRequest(query);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchView.setFocusable(true);
                return false;
            }
        });
    }

    private void setupAdapterAndRecycler() {
        itemRv = findViewById(R.id.recipe_rv);
        itemRv.setLayoutManager(new LinearLayoutManager(this));
        recipeAdapter = new RecipeAdapter(recipeModels);
        recipeAdapter.setListener(this);
        itemRv.setAdapter(recipeAdapter);
    }

    private void setToolbarTitle() {
        setBaseTitle(getString(R.string.recipe_labs));
    }

    protected void setBaseTitle(@NonNull String text) {
        title.setText(text);
    }

    @Override
    public void onItemClicked(@NonNull String url) {
        if (connectionManager.isNetworkConnected()) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } else {
            showToast("Check your internet connection");
        }
    }
}
