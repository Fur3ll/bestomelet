package com.example.vicktor.bestomelet.networking;

public final class ApiConstance {

    private static final String BASE_URL = "http://www.recipepuppy.com/";
    private static final String API_VER = "api/";

    public static final String BASE_REQUEST = BASE_URL + API_VER;
}
