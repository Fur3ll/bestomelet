package com.example.vicktor.bestomelet.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vicktor.bestomelet.R;
import com.example.vicktor.bestomelet.model.RecipeModel;
import com.example.vicktor.bestomelet.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeVH> {

    private List<RecipeModel> recipe;
    private OnRecipeItemClickListener listener;

    private static  String EMPTY_THUMBNAIL = "http://img.recipepuppy.com/9.jpg";

    public interface OnRecipeItemClickListener {
        void onItemClicked(@NonNull String url);
    }

    public void setListener(OnRecipeItemClickListener listener) {
        this.listener = listener;
    }

    public RecipeAdapter(@NonNull List<RecipeModel> recipe) {
        this.recipe = recipe;
    }

    @Override
    public RecipeVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe, parent, false);
        return new RecipeVH(v);
    }

    @Override
    public void onBindViewHolder(final RecipeVH holder, int position) {
        holder.bind(recipe.get(position));
    }

    @Override
    public int getItemCount() {
        return recipe.size();
    }

    class RecipeVH extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView image;
        TextView ingredients;
        TextView title;

        public RecipeVH(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.recipe_cv);
            image = (ImageView) itemView.findViewById(R.id.image_iv);
            ingredients = (TextView) itemView.findViewById(R.id.ingredients_tv);
            title = (TextView) itemView.findViewById(R.id.title_tv);
            ButterKnife.bind(this, itemView);
        }

        public void bind(@NonNull RecipeModel model) {
            title.setText(model.getTitle());
            ingredients.setText(model.getIngredients());

            if (model.getThumbnail() != null && !model.getThumbnail().isEmpty()){
                Picasso.with(image.getContext())
                        .load(model.getThumbnail())
                        .transform(new CircleTransform())
                        .fit()
                        .centerInside()
                        .into(image);
            } else {
                Picasso.with(image.getContext())
                        .load(EMPTY_THUMBNAIL)
                        .transform(new CircleTransform())
                        .fit()
                        .centerInside()
                        .into(image);
            }

            final String url = model.getHref();
            clickOnItem(url);
        }

        private void clickOnItem(@NonNull final String url) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemClicked(url);
                    }
                }
            });
        }
    }
}
