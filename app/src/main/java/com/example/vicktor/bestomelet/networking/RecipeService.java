package com.example.vicktor.bestomelet.networking;

import com.example.vicktor.bestomelet.model.RecipeResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.example.vicktor.bestomelet.networking.ApiConstance.BASE_REQUEST;

public interface RecipeService {

    @GET(BASE_REQUEST)
    Call<RecipeResponse> getRecipes(@Query("i") String ingredients,
                                    @Query("q") String searchParam,
                                    @Query("p") String page);

    @GET(BASE_REQUEST)
    Call<RecipeResponse> getRecipe(@Query("q") String searchParam);

}
