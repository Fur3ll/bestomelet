package com.example.vicktor.bestomelet.networking.request.recipeRequest;

import android.support.annotation.NonNull;

import com.example.vicktor.bestomelet.model.RecipeResponse;
import com.example.vicktor.bestomelet.networking.NetworkHelper;
import com.example.vicktor.bestomelet.networking.request.BaseRequest;

import retrofit2.Call;

public class SearchRecipeRequest extends BaseRequest<RecipeResponse> {

    private String searchParam;

    public SearchRecipeRequest(@NonNull String searchParam) {
        this.searchParam = searchParam;
    }

    @Override
    public Call<RecipeResponse> getCall() {
        return NetworkHelper.getInstance().getRecipeService().getRecipe(searchParam);
    }
}