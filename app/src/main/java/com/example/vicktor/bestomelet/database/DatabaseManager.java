package com.example.vicktor.bestomelet.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.example.vicktor.bestomelet.model.RecipeModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private static final String RECIPES = "Recipes";
    private static final String TITLE = "title";
    private static final String HREF = "href";
    private static final String INGREDIENTS = "ingredients";
    private static final String THUMBNAIL = "thumbnail";

    public static void saveRecipes(@NonNull Context context, @NonNull RecipeModel model) {
        DatabaseHelper helper = new DatabaseHelper(context);
        SQLiteDatabase database = helper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, model.getTitle());
        contentValues.put(HREF, model.getHref());
        contentValues.put(INGREDIENTS, model.getIngredients());
        contentValues.put(THUMBNAIL, model.getThumbnail());
        database.insert(RECIPES, null, contentValues);
    }

    public static List<RecipeModel> getSearchResult(@NonNull Context context, @NonNull String search) {
        List<RecipeModel> list = new ArrayList<>();
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        String rawQuerySelection = null;
        String query = search;
        rawQuerySelection = "SELECT * From " + RECIPES
                + " WHERE " + TITLE + " LIKE "
                + "'%"
                + query
                + "%' COLLATE NOCASE";

        Cursor cursor = database.rawQuery(rawQuerySelection, null);
        if (cursor.moveToFirst()) {
            do {
                RecipeModel model = generateModel(cursor);
                list.add(model);
            } while (cursor.moveToNext());
        } else {
            cursor.close();
        }
        cursor.close();
        database.close();
        return list;
    }

    @NonNull
    private static RecipeModel generateModel(@NonNull Cursor cursor) {
        RecipeModel model = new RecipeModel();
        model.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
        model.setHref(cursor.getString(cursor.getColumnIndex(HREF)));
        model.setIngredients(cursor.getString(cursor.getColumnIndex(INGREDIENTS)));
        model.setThumbnail(cursor.getString(cursor.getColumnIndex(THUMBNAIL)));
        return model;
    }

    public static List<RecipeModel> getAllRecipes(@NonNull Context context) {
        List<RecipeModel> list = new ArrayList<>();
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        Cursor cursor = database.query(RECIPES, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                RecipeModel model = generateModel(cursor);
                list.add(model);
            } while (cursor.moveToNext());
        } else {
            cursor.close();
        }

        return list;
    }

}
