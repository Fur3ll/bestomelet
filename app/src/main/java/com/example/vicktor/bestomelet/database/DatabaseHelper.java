package com.example.vicktor.bestomelet.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "recipe_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Recipes");
        createTable(db);
    }

    private void createTable(SQLiteDatabase database) {
        database.execSQL("create table Recipes"
                + " ("
                + "id integer primary key autoincrement,"
                + "title text UNIQUE ON CONFLICT REPLACE,"
                + "href text,"
                + "ingredients text,"
                + "thumbnail text"
                + ");");
    }

}
